# Install Linux & Node  (Ubuntu 14.0.4 is officially supported version from Salesforce)
FROM node:alpine

LABEL maintainer="LogRhythm Global IT <helpme@logrhythm.com>"

# Install Dependencies
RUN apk add --update bash git curl jq libxml2-utils

# Install Salesforce CLI
RUN npm install sfdx-cli --global
RUN sfdx --version
RUN sfdx plugins --core